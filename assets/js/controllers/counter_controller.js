import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = ['counter'];

    decrement() {
        this.counterTarget.innerText = this.counter > 0 ? this.counter - 1 : 0;
    }

    increment() {
        this.counterTarget.innerText = this.counter + 1;
    }

    get counter() {
        return Number(this.counterTarget.innerText);
    }
}
