<?php

use SibWP\AssetResolver;

add_action('wp_enqueue_scripts', function () {

	// enqueue global assets
	wp_enqueue_style('app', AssetResolver::resolve('css/app.css'), [], false);
	wp_enqueue_script('appjs', AssetResolver::resolve('js/app.js'), [], false);

	// remove gutenberg styles
	wp_dequeue_style('wp-block-library');

	// remove jquery
	if (!is_user_logged_in()) {
		wp_deregister_script('jquery');
		wp_deregister_script('wp-embed');
	}
});

/**
 * defer bundle script
 */
add_filter(
	'script_loader_tag',
	function ($tag, $handle, $src) {
		$defer_scripts = [
			'appjs'
		];

		if (in_array($handle, $defer_scripts)) {
			return '<script src="' . $src . '" defer="defer" type="text/javascript" data-turbolinks-track="reload"></script>' . "\n";
		}
		return $tag;
	},
	10,
	3
);

/* 
* remove emoji stuff
*/
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');
