<?php

// register Menu
add_action('after_setup_theme', function () {
    register_nav_menu('primary', __('Mainmenu', 'Main Menu'));
    register_nav_menu('secondary', __('Footermenu', 'Footer Menu'));
});


/*
don't strip tags from tinymce
*/
add_filter('tiny_mce_before_init', function ($initArray) {
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
});


/*
add active class to current page & accept classes for li children
*/
add_filter('nav_menu_css_class', function ($classes, $item, $args) {
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'text-teal-500 ';
    }

    // accept "add_li_class" in wp_nav_menu
    if (isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }

    return $classes;
}, 10, 3);
