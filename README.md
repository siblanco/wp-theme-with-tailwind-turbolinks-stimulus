Wordpress Starter theme with [tailwind](https://tailwindcss.com/), [turbolinks](https://github.com/turbolinks/turbolinks) and [stimulus](https://github.com/stimulusjs/stimulus). Development follows a component style pattern.

This theme is inspired by / partly copied from [mishterk's starter](https://github.com/mishterk/wp-tailwindcss-theme-boilerplate). (Also some of this readme xD).

## Getting started:

1. Clone into an empty theme directory
2. Run `npm install`
3. Search & Replace 'SibWP' with your own namespace
4. Change the proxy uri to your local dev url in `webpack.mix.js`
5. Change the theme information in `style.css`
6. Activate your theme

Have a look in the file structure to see how I am doing things. Basically index.php uses `get_template_part` and `get_post_type` to figure out the correct partial to render. These partials render content as well as components:

```
<section class="container">
    Some stuff specific to posts with type "page"

    <!-- reusable components -->
    <?php \SibWP\Components::render('PageIntro', [
        'title' => get_the_title()
    ]); ?>
</section>
```

The class Components accepts the Component name which is a folder in `templates/components/[component-name]` and expects an `index.php` in there. You can change that up, but I am using folders in case I want to split the component into more files.

The Counter component (`templates/components/Counter` and `assets/js/controllers/counter_controller.js`) shows you how to get started with Stimulus. Read their docs for more.

## Commands

#### `npm start`

Start local server with hmr
(routing with Turbolinks doesn't work while on localhost, so you got to refresh once you change routes or disable turbolinks while developing).

#### `npm run build:dev`

Runs the development build

#### `npm run build`

Runs the production build which includes asset file versioning and Purge CSS
