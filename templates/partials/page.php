<section class="container">
    Some stuff specific to posts with type "page"

    <!-- reusable components -->
    <?php \SibWP\Components::render('PageIntro', [
        'title' => get_the_title()
    ]); ?>

</section>