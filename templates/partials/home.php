<?php

use SibWP\Components;

?>

<section class="container">
	<?php
	Components::render('PageIntro', ['title' => get_the_title()]);
	Components::render('Counter');
	?>
</section>