<div data-controller="counter" class="mt-4">
    <p data-target="counter.counter">0</p>
    <button data-action="click->counter#increment" class="bg-teal-500 p-2 rounded-md text-white">
        Increment
    </button>
    <button data-action="click->counter#decrement" class="bg-red-500 p-2 rounded-md text-white">
        Decrement
    </button>
</div>