<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="profile" href="https://gmpg.org/xfn/11" />

    <title><?php wp_title(); ?></title>

    <!-- Google Analytics -->
    <?php if (isset($_COOKIE['gaAllowed']) && $_COOKIE['gaAllowed'] == 1) : ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-XXXXXXX-X"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-XXXXXXX-X', {
                'anonymize_ip': true
            });
        </script>
    <?php endif;

    wp_head(); ?>
</head>

<body <?php body_class() ?>>

    <header class="container mb-4 mt-4">
        <nav>
            <?php wp_nav_menu($args = array(
                'menu' => 'mainmenu',
                'menu_class' => 'flex space-x-4',
                'add_li_class' => 'font-display font-bold ',
                'container' => false
            )); ?>
        </nav>
    </header>