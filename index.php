<?php
get_header();

$partType = is_front_page() ? 'home' : get_post_type();
get_template_part('templates/partials/' . $partType);

get_footer();
