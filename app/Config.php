<?php

namespace SibWP;

class Config
{
    public static function init()
    {
        define('THEMEDIR', get_stylesheet_directory());
        define('THEMEURL', get_stylesheet_directory_uri());
    }
}
