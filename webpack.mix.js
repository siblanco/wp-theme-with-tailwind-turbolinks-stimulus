const mix = require('laravel-mix');

const purgecss = require('@fullhuman/postcss-purgecss')({
    content: [
        './*.php',
        './templates/**/*.php',
        './components/**/*.php',
        './components/**/*.html',
        './src/**/*.css',
        './src/**/*.js'
    ],

    defaultExtractor: content => {
        const broadMatches = content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || [];
        const innerMatches =
            content.match(/[^<>"'`\s.()]*[^<>"'`\s.():]/g) || [];

        return broadMatches.concat(innerMatches);
    }
});

mix.setPublicPath('./build');

mix.browserSync({
    proxy: 'http://your-localdev-url/',
    injectChanges: true,
    open: false,
    files: ['build/**/*.{css,js}', 'templates/**/*.php', './*.php']
});

mix.js('assets/js/app.js', 'js').postCss('assets/css/app.css', 'css', [
    require('postcss-import'),
    require('tailwindcss')('./tailwind.js'),
    require('autoprefixer'),
    ...(process.env.NODE_ENV === 'production' ? [purgecss] : [])
]);

if (mix.inProduction()) {
    require('laravel-mix-versionhash');
    mix.versionHash();
    mix.sourceMaps();
}
