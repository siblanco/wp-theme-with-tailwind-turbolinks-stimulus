<?php

use SibWP\AutoLoader;
use SibWP\Components;
use SibWP\Config;

/*
 * Set up our auto loading class and mapping our namespace to the app directory.
 *
 * The autoloader follows PSR4 autoloading standards so, provided StudlyCaps are used for class, file, and directory
 * names, any class placed within the app directory will be autoloaded.
 *
 * i.e; If a class named SomeClass is stored in app/SomeDir/SomeClass.php, there is no need to include/require that file
 * as the autoloader will handle that for you.
 */

require get_stylesheet_directory() . '/app/AutoLoader.php';
$loader = new AutoLoader();
$loader->register();
$loader->addNamespace('SibWP', get_stylesheet_directory() . '/app');

// init global variables
Config::init();

// Set Components Dir
Components::$componentDir = THEMEDIR . '/templates/components';

require THEMEDIR . '/includes/theme-setup.php';
require THEMEDIR . '/includes/scripts-and-styles.php';
